using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace RomanNumeralChallenge.Services
{
    public class RomanNumeralConverter
    {
        public string Convert(int arabic)
        {
            var uniqueCharacters = new Dictionary<int, string>
            {
                {1, "I" }, {5, "V" }, {10, "X" }, {50, "L" }, {100, "C" }, {500, "D" }, {1000, "M" }
            };

            foreach (var key in uniqueCharacters.Keys)
            {
                var lessThanDifference = key - arabic;
                var incrementConcat = "";

                var greaterByOne = key + 1;

                if (arabic == key)
                    return uniqueCharacters[key];

                if (lessThanDifference == 1 && arabic > 0)
                    return "I" + uniqueCharacters[key];

                // increment the I...
                for (int i = 0; i <= lessThanDifference; i++)
                {
                    if (arabic > 0)
                        incrementConcat += "I";
                }

                if (incrementConcat.Length > 0)
                    return incrementConcat;
                
                if (arabic == greaterByOne)
                    return uniqueCharacters[key] + "I";
            }

            return "";
        }
    }
}