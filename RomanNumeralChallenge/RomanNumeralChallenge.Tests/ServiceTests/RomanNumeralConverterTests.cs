﻿using NUnit.Framework;
using RomanNumeralChallenge.Services;

namespace RomanNumeralChallenge.Tests.ServiceTests
{
    [TestFixture]
    public class RomanNumeralConverterTests
    {
        [Test]
        public void ConvertArabicToRoman_NumberSuppliedMatchesUniqueCharacters_ReturnsCorrectRomanNumeral()
        {
            Assert.AreEqual("I", new RomanNumeralConverter().Convert(1));
            Assert.AreEqual("V", new RomanNumeralConverter().Convert(5));
            Assert.AreEqual("X", new RomanNumeralConverter().Convert(10));
            Assert.AreEqual("L", new RomanNumeralConverter().Convert(50));
            Assert.AreEqual("C", new RomanNumeralConverter().Convert(100));
            Assert.AreEqual("D", new RomanNumeralConverter().Convert(500));
            Assert.AreEqual("M", new RomanNumeralConverter().Convert(1000));
        }

        [Test]
        public void ConvertArabicToRoman_NumberSuppliedLessThanUnique_ReturnsRomanNumeral()
        {
            Assert.AreEqual("I", new RomanNumeralConverter().Convert(1));
            Assert.AreEqual("II", new RomanNumeralConverter().Convert(2));
            Assert.AreEqual("III", new RomanNumeralConverter().Convert(3));
            Assert.AreEqual("IV", new RomanNumeralConverter().Convert(4));
            Assert.AreEqual("IX", new RomanNumeralConverter().Convert(9));
            Assert.AreEqual("IL", new RomanNumeralConverter().Convert(49));
            Assert.AreEqual("IC", new RomanNumeralConverter().Convert(99));
            Assert.AreEqual("ID", new RomanNumeralConverter().Convert(499));
            Assert.AreEqual("IM", new RomanNumeralConverter().Convert(999));
        }

        [Test]
        public void ConvertArabicToRoman_NumberSuppliedGreaterThanUniqueByOne_ReturnsRomanNumeral()
        {
            Assert.AreEqual("VI", new RomanNumeralConverter().Convert(6));
            Assert.AreEqual("XI", new RomanNumeralConverter().Convert(11));
            Assert.AreEqual("LI", new RomanNumeralConverter().Convert(51));
            Assert.AreEqual("CI", new RomanNumeralConverter().Convert(101));
            Assert.AreEqual("DI", new RomanNumeralConverter().Convert(501));
            Assert.AreEqual("MI", new RomanNumeralConverter().Convert(1001));
        }

        [Test]
        public void ConvertArabicToRoman_InvalidNumbersSupplied_ReturnsEmptyString()
        {
            Assert.AreEqual("", new RomanNumeralConverter().Convert(0), "0 should be ''");
            Assert.AreEqual("", new RomanNumeralConverter().Convert(-1), "-1 should be ''");
            Assert.AreEqual("", new RomanNumeralConverter().Convert(-100), "-100 should be ''");
        }
    }
}
